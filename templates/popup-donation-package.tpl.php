<div class="donation-popup zoom-anim-dialog mfp-hide popup-<?php echo $package_id; ?>" id="donation-popup-<?php echo $package_id; ?>">
<!-- Begin MailChimp Signup Form -->
<img width="230" height="50" src="<?php echo CNC_DONATION_PLUGIN_URL; ?>/assets/images/TI_logo_hrl.png" alt="Transparency International logo" class="logo" />
<div class="donation-package">
<form action="<?php echo get_site_url(); ?>/cnc-donation" method="post" id="" name="donation-package-form" class="">
  <div id="mc_embed_signup_scroll">
    <h3 class="title"><?php echo $package_name; ?></h3>
    <div class="mc-field-group form-group">
      <label class="sr-only" for="mce-MMERGE3"><?php _e('Name', 'cnc-donation'); ?> </label>
      <input type="text" value="" name="supporter-name" class="form-control input-lg input-name" id="mce-MMERGE3" placeholder="<?php _e('Name'); ?>">
      <div class="help-block"><?php _e('Missing name', 'cnc-donation'); ?></div>
    </div>
    <div class="mc-field-group form-group">
      <label class="sr-only" for="mce-EMAIL-<?php echo $package_id; ?>"><?php _e('Email', 'cnc-donation'); ?> </label>
      <input type="email" value="" name="supporter-email" class="required email form-control input-lg" id="mce-EMAIL-<?php echo $package_id; ?>" placeholder="<?php _e('Email'); ?>">
      <div class="help-block"><?php _e('Missing Email', 'cnc-donation'); ?></div>
    </div>
    <div class="mc-field-group form-group">

        <label><?php _e('Provider', 'cnc-donation'); ?></label>
        <div id="provider-<?php echo $package_id; ?>">
            <label class="radio-inline">
                <input type="radio" name="provider" checked value="Barion2" class="required">
                <span>Barion</span><img src="<?php echo CNC_DONATION_PLUGIN_URL; ?>/assets/images/barion.svg">
            </label>
            <label class="radio-inline">
                <input type="radio" name="provider" value="Paypal" class="required">
                <span>Paypal</span><img src="<?php echo CNC_DONATION_PLUGIN_URL; ?>/assets/images/paypal.svg">
            </label>
        </div>
    </div>
<!--<div class="mc-field-group form-group text-left">
      <label class="custom-control custom-checkbox">
        <input type="checkbox" class="custom-control-input">
        <span class="custom-control-indicator"></span>
        <span class="custom-control-description">I would like to sign up for the Transparency Newsletter</span>
      </label>
    </div> -->
    <div class="mc-field-group form-group text-left">
      <label class="custom-control custom-checkbox">
        <input type="checkbox" class="custom-control-input terms-accept">
        <span class="custom-control-indicator"></span>
        <span class="custom-control-description"><?php _e('I accept the <a href="/en/terms-and-conditions" target="_blank">terms and conditions</a>', 'cnc-donation'); ?></span>
      </label>
      <div class="help-block"><?php _e('You have to accept the terms and conditions.', 'cnc-donation'); ?></div>
    </div>
    <div class="clear form-group-submit">
      <input type="hidden" class="cnc-package-id" name="cnc-package-id" value="<?php echo $package_id; ?>">
      <input type="submit" value="<?php _e('I support Transparency International Hungary!', 'cnc-donation'); ?>" name="donation-submitted" id="mc-embedded-subscribe" class="button btn btn-primary">
    </div>
  </div>
  <!-- <p class="footnote"><?php _e('You can cancel your subscription any time.', 'cnc-donation'); ?></p> -->
</form>
</div>

<!--End mc_embed_signup-->
</div>
