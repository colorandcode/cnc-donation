<p><img src="https://transparency.colorandcode.hu/wp-content/themes/sage/dist/images/logo-hu.png" alt="Transparency Internationsl logo"></p>
<p>Kedves Támogatónk!</p>

<p>Nagyon köszönjük, hogy az eddigiekben rendszeres adományoddal hozzájárultál működésünkhöz! Ezúton visszaigazoljuk, hogy a rendszeres fizetésed leállítása sikeres volt.<br>
Amennyiben meggondolnád magad, továbbra is örömmel vesszük a támogatásod. A különböző támogatási lehetőségekről <a href="https://transparency.hu/adomanyozas/">ezen a címen</a> tájékozódhatsz.</p>

<p>Köszönettel és üdvözlettel:<br>
a TI Magyarország csapata</p>


