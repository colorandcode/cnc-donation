<p><img src="https://transparency.colorandcode.hu/wp-content/themes/sage/dist/images/logo-hu.png" alt="Transparency Internationsl logo"></p>

<p>Dear Supporter,
Thank you very much for contributing to the operation of our organization with your recurring donation! We hereby confirm that the recurring payment was successfully initiated.</p>

<p>Your donation is an invaluable contribution to support our work. However, if you change your mind, you may terminate your recurring donation anytime at:<br>
<a href="<?php echo get_site_url(); ?>/en/cnc-donation/?donation_id=<?php echo $transaction->transaction_id ?>" >
    My donation status
</a></p>

<p>Please keep this email for future reference. You can find more information about donation options <a href="https://transparency.hu/en/donation/">at this address</a>.</p>

<p>Thank you and kind regards:<br>
the TI Hungary team</p>
