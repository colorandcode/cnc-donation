<p><img src="https://transparency.colorandcode.hu/wp-content/themes/sage/dist/images/logo-hu.png" alt="Transparency Internationsl logo"></p>

<p>Kedves Támogatónk!</p>

<p>Nagyon köszönjük, hogy rendszeres adományoddal hozzájárulsz szervezetünk működéséhez! Ezúton visszaigazoljuk, hogy a rendszeres fizetés elindítása sikeres volt.</p>

<p>Adományod nagy segítséget jelent számunkra ahhoz, hogy folytathassunk a munkánkat. Ha azonban mégis meggondolnád magad, rendszeres támogatásodat bármikor leállíthatod az alábbi címen:<br>
<a href="<?php echo get_site_url(); ?>/cnc-donation/?donation_id=<?php echo $transaction->transaction_id ?>" >
    Támogatásom állapota
</a></p>

<p>Kérjük, őrizd meg ezt a levelet annak érdekében, hogy a leiratkozással kapcsolatos információ a későbbiekben is rendelkezésedre álljon.
A további támogatási lehetőségekről <a href="https://transparency.hu/adomanyozas/">ezen a címen</a> tájékozódhatsz.</p>

<p>Köszönettel és üdvözlettel:<br>
a TI Magyarország csapata</p>

