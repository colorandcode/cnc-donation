<div class="payment-packages">
	<div class="payment-packages-wrap row">
		<div class="package package-1 col-md-4 col-sm-6">
			<div class="item-wrap">
				<p class="title"><?php _e('Ally of Transparency', 'cnc-donation'); ?></p>
				<p class="amount">2500 <?php _e('HUF', 'cnc-donation'); ?></p>
				<p class="description"><?php _e('If you support us with a monthly donation of HUF 2,500 apart from our regular newsletters you will also receive our press releases the moment they are published.', 'cnc-donation'); ?></p>
			</div>
			<a class="package-selector" data-id="1" href="<?php echo get_site_url(); ?>/adomanyozas-static"></a>
		</div>
		<div class="package package-2 col-md-4 col-sm-6">
			<div class="item-wrap">
				<p class="title"><?php _e('Champion of Integrity', 'cnc-donation'); ?></p>
				<p class="amount">5000 <?php _e('HUF', 'cnc-donation'); ?></p>
				<p class="description"><?php _e('With a monthly donation of HUF 5,000 in addition to our newsletters and press releases we will also send you personalized invitations to our events where you can meet our team, our experts, and the speakers at each event. ', 'cnc-donation'); ?></p>
			</div>
			<a class="package-selector" data-id="2" href="<?php echo get_site_url(); ?>/adomanyozas-static"></a>
		</div>
		<div class="package package-3 col-md-4 col-sm-6">
			<div class="item-wrap">
				<p class="title"><?php _e('Anti-Corruption Superhero', 'cnc-donation'); ?></p>
				<p class="amount">10000 <?php _e('HUF', 'cnc-donation'); ?></p>
				<p class="description"><?php _e('If you support us with a monthly donation of HUF 10,000, besides receiving our newletters, our press releases, and personalized invitations to our events, you will also be invited to our annual reception held on the International Anti-Corruption Day (December 9). Moreover, you will also receive a TI gift package delivered to your address.', 'cnc-donation'); ?></p>
			</div>
			<a class="package-selector" data-id="3" href="<?php echo get_site_url(); ?>/adomanyozas-static"></a>
		</div>
	</div>
</div>