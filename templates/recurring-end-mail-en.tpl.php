<p><img src="https://transparency.hu/wp-content/themes/sage/dist/images/logo-hu.png" alt="Transparency Internationsl logo"></p>
<p>Dear Supporter,</p>

<p>Thank you very much for having contributed to our operation with your recurring donation so far. We hereby confirm that your recurring payment was successfully terminated.<br>
If you change your mind, we welcome your support anytime. You can find more information about donation options at <a href="https://transparency.hu/en/donation/">this address</a>.</p>

<p>Thank you and kind regards:<br>
the TI Hungary team</p>
