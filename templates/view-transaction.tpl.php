<section class="widget">
    <h3><?php _e('Recurring donation','cnc-donation'); ?></h3>
    <p><?php _e('Thank you for your recurring donation to Transparency International Hungary. At this page, you may review the status of your recurring payment, as well as terminate your payment if you wish.','cnc-donation') ?></p>
    <p><?php _e('Should you decide to terminate your recurring payment, a confirmation will be sent to you in email.','cnc-donation') ?></p>
    <div class="col-md-12">
        <dl class="row">
            <dt class="col-sm-3"><?php _e('Status','cnc-donation'); ?></dt>
            <dd class="col-sm-9"><?php _e($transaction->status,'cnc-donation'); ?></dd>
            <?php __('stopped', 'cnc-donation'); // for translation hack ?>
            <?php __('successful', 'cnc-donation'); // for translation hack ?>

            <dt class="col-sm-3"><?php _e('Donation amount','cnc-donation'); ?></dt>
            <dd class="col-sm-9"><?php echo $transaction->amount . ' HUF'; ?></dd>

            <dt class="col-sm-3"><?php _e('First transaction','cnc-donation'); ?></dt>
            <dd class="col-sm-9"><?php echo $transaction->tdate ?></dd>

            <dt class="col-sm-3"><?php _e('Last transaction','cnc-donation'); ?></dt>
            <dd class="col-sm-9"><?php echo $transaction->ldate ?></dd>
        </dl>
        <?php if($transaction->status == 'successful') : ?>
        <form action="<?php echo get_site_url(); ?>/cnc-donation/?donation_id=<?php echo $transaction->transaction_id ?>" method="post">
            <input type="hidden" name="stop_donation" value="1" />
            <button type="submit" class="btn btn-warning"><?php _e('Stop donation','cnc-donation'); ?></button>
        </form>
        <?php endif; ?>
    </div>
</section>
