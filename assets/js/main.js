(function($) {

  var form_intact = true;

  // Email input validator
  function validateEmail($email) {
    var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
    return emailReg.test( $email );
  }

  /**
   * Check is donation form validation rules matching
   * @param {jQuery Object} $form Parent form object
   * @returns {bool} Is validated
   */
  function validateNlForm($form) {
    $('.donation-popup .form-group').removeClass('has-error');
    var $email = $form.find(".form-control.email");
    var $name = $form.find(".form-control.input-name");
    var $terms = $form.find(".custom-control-input.terms-accept");
    var $package = $form.find("input.cnc-package-id");
    var packageId = $package.val();
    var $donate_recurring = $form.find(".input-donate-recurring");
    var $provider = $form.find("#provider-" + packageId + " input[name=provider]:checked");
    var isPaypal = ($provider.val() == 'Paypal');
    var hasRecurringAmount = !!$donate_recurring.val();

    var email_required = (!isPaypal && (packageId != 4)) ||  (!isPaypal && hasRecurringAmount);

    var validate = true;
    if ((email_required && !$email.val()) || !validateEmail($email.val())) {
      $email.parent().addClass('has-error');
      validate = false;
    }

    if (!$terms.is(':checked')) {
    	$terms.parent().parent().addClass('has-error');
      validate = false;
    }
    return validate;
  }

  function validateDonationAmount() {
    var $donate_single = $(".donation-popup .input-donate-single");
    var $donate_recurring = $(".donation-popup .input-donate-recurring");
    var validate = true;
    if ((!$donate_single.val() && !$donate_recurring.val()) || ($donate_single.val() && $donate_single.val() < 1000)) {
      $donate_single.parent().addClass('has-error');
      $donate_recurring.parent().addClass('has-error');
      validate = false;
    }
    return validate;
  }

  function handleMultipleAmounts() {
    var $donate_single = $(".donation-popup .input-donate-single");
    var $donate_recurring = $(".donation-popup .input-donate-recurring");
    $donate_single.keyup(function() {
    	$donate_recurring.val('');
    });
    $donate_recurring.keyup(function() {
    	$donate_single.val('');
    });
    $donate_single.change(function() {
    	$donate_recurring.val('');
    });
    $donate_recurring.change(function() {
    	$donate_single.val('');
    });
  }

  function donation_ga_events($package_id) {
    switch(parseInt($package_id.val())) {
      case 1:
        ga('send', {
          hitType: 'event',
          eventCategory: 'paypal_adomany',
          eventAction: 'click',
          eventLabel: '2500_ft'
        });
        break;
      case 2:
        ga('send', {
          hitType: 'event',
          eventCategory: 'paypal_adomany',
          eventAction: 'click',
          eventLabel: '5000_ft'
        });
        break;
      case 3:
        ga('send', {
          hitType: 'event',
          eventCategory: 'paypal_adomany',
          eventAction: 'click',
          eventLabel: '10000_ft'
        });
      case 4:
        ga('send', {
          hitType: 'event',
          eventCategory: 'paypal_adomany',
          eventAction: 'click',
          eventLabel: 'egyedi_adomany'
        });
        break;
    }
  }

	function init_donation_popup() {
		$('.payment-packages .package-selector').click(function (e) {
			e.preventDefault();
			// var donation_id = $(this).data('id');

			// $.magnificPopup.open({
			// 	items: {
			// 		src: '#donation-popup-' + donation_id,
			// 		type: 'inline',

			// 		fixedContentPos: false,
			// 		fixedBgPos: true,

			// 		overflowY: 'auto',

			// 		closeBtnInside: true,
			// 		preloader: false,

			// 		midClick: true,
			// 		removalDelay: 300,
			// 		mainClass: 'my-mfp-slide-bottom'
			// 	}
			// });
		});

		$('.donation-popup form').submit(function(e) {
			if(!validateNlForm($(this))) {
				e.preventDefault();
			}
      //donation_ga_events($(this).find('.cnc-package-id'));
		});

		$('.donation-popup form.indie').submit(function(e) {
			if(!validateDonationAmount()) {
				e.preventDefault();
			}
      form_intact = false;
		});

		$('.donation-popup form input').focusout(function(e) {
      if (!form_intact) {
        //validateNlForm();
      }
		});

		handleMultipleAmounts();
	}

	init_donation_popup();

})(jQuery);